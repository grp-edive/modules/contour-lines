# Contour Lines Environment

You can just clone this into the non-versioned `Environments` folder in the eDIVE core repository (or any other non-versioned folder). This can be done through eDIVE Project Setup window if setup correctly. The main scene should become available in the scene switch window or you can open it manualy.
This module contains the shaders required to display contour lines on an arbitrary 3D object. It also contains the scripts related working with the terrains as well as the monolithic script responsible for the synchronization of the terrain state and related UI.

When cloning manually, must be cloned into a folder called `VRstevnice` otherwise the metadata will break.

## Authors and acknowledgment
SW Design and Implementation:
* Vojtěch Brůža (FI MUNI)
* Jiří Chmelík (FI MUNI)

The development of this SW was supported by project [EduInCIVE](https://edive.muni.cz/) -- a research project of [Masaryk University](https://www.muni.cz/), funded by the [Technology Agency of the Czech Republic](https://www.tacr.cz/en/).

## License
This software is licensed under the [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.en.html). See Licence.md for full text of licence.