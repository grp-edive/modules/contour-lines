﻿using Edive.Environments.TerrainContours.Flags;
using UnityEditor;
using UnityEngine;

namespace Edive.Environments.TerrainContours
{
    [CustomEditor(typeof(FlagManager))]
    [CanEditMultipleObjects]
    public class FlagManagerEditor : Editor
    {
        FlagManager myScript;
        private void OnEnable()
        {
            myScript = (FlagManager)target;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GUILayout.Button("Check objective"))
            {
                Debug.Log("Flag visible: " + myScript.CheckLineVisibilityThroughAllFlags());
            }
        }
    }
}
