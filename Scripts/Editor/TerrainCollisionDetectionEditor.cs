﻿using Edive.Environments.TerrainContours.Utils;
using UnityEditor;
using UnityEngine;

namespace Edive.Environments.TerrainContours
{
    [CustomEditor(typeof(TerrainCollisionDetection))]
    [CanEditMultipleObjects]
    public class TerrainCollisionDetectionEditor : Editor
    {
        TerrainCollisionDetection myScript;
        private void OnEnable()
        {
            myScript = (TerrainCollisionDetection)target;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GUILayout.Button("Compute Intervisibility"))
            {
                Debug.Log("Visible: " + myScript.Intervisibility());
            }
        }
    }
}
