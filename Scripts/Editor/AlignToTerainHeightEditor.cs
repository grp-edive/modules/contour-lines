﻿using Edive.Environments.TerrainContours.Utils;
using UnityEditor;
using UnityEngine;

namespace Edive.Environments.TerrainContours
{
    [CustomEditor(typeof(AlignToTerainHeight))]
    [CanEditMultipleObjects]
    public class AlignToTerainHeightEditor : Editor
    {
        AlignToTerainHeight myScript;
        private void OnEnable()
        {
            myScript = (AlignToTerainHeight)target;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GUILayout.Button("Align"))
            {
                myScript.TryToAlign();
            }
        }
    }
}
