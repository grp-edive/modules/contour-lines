﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Edive.Environments.TerrainContours.Terrain
{
    public class ContourMap : MonoBehaviour
    {
        [SerializeField]
        private Color ortophotoContourColor = Color.yellow;

        [SerializeField]
        private Color blankContourColor = Color.clear;

        [SerializeField]
        private ContourMapTile[] mapTiles;

        [SerializeField]
        private string cropParameterName = "_ClampTop";

        [SerializeField]
        private string flatnessParameterName = "_Flatness";

        [SerializeField]
        private string equidistanceParameterName = "_YStep";

        [SerializeField]
        private bool initBlankInStart = false;

        public float toggle3DanimationTime = 1;

        public bool isBlank { get; private set; }

        public bool is2D { get; private set; }

        private IEnumerator animationCoroutine;
        private string runningAnimation = "";
        private float elapsedAnimationTime = 0;
        private UnityEvent animationFinished = new UnityEvent();

        /// <summary>
        /// Value in range (1, 10). Remmaped to (0.001, 0.01) when setting the shader value.
        /// </summary>
        private float equidistance = 3;

        /// <summary>
        /// Value in range (0, 1).
        /// </summary>
        private float cropTop = 1;

        private void Start()
        {
            if (mapTiles.Length == 0)
            {
                Debug.LogError("Empty map.");
            }

            if (initBlankInStart) Init();
        }

        private void OnEnable()
        {
            if (isBlank) SwitchToBlank();
            else SwitchToOrthophoto();
            if (is2D) SwitchTo2D();
            else SwitchTo3D();
        }

        /// <summary>
        /// Correctly setups the initial state of the maps.
        /// To be able to use it with Mirror.
        /// </summary>
        /// <param name="blank"></param>
        /// <param name="flat"></param>
        public void Init(bool blank = true, bool flat = true)
        {
            initBlankInStart = false;
            foreach (var tile in mapTiles)
            {
                tile.Init();
            }

            if (flat) Reset2D();
            else Reset3D();
            if (blank) SwitchToBlank();
            else SwitchToOrthophoto();
        }

        public void Reset3D()
        {
            SetMaterialProperty(flatnessParameterName, 0);
            SetLit(true);
            is2D = false;
        }

        public void ResetMap()
        {
            Reset2D();
            SwitchToBlank();
        }

        public void Reset2D()
        {
            // flatten the map at start (cannot be dont in awake, because the materials are not yet instantiated
            SetMaterialProperty(flatnessParameterName, 1);
            SetLit(false);
            is2D = true;
        }

        public void SwitchToOrthophoto()
        {
            foreach (var m in mapTiles)
            {
                m.SetContourColor(ortophotoContourColor);
                m.SetTextureOrtophoto();
            }
            isBlank = false;
        }

        public float GetCropValue()
        {
            return cropTop;
        }

        public float GetEquidistance()
        {
            return equidistance;
        }

        public void SwitchToBlank()
        {
            foreach (var m in mapTiles)
            {
                m.SetContourColor(blankContourColor);
                if (is2D)
                {
                    // 2D texture contours
                    m.SetTextureBlank();
                }
                else
                {
                    // 3D computed contours
                    m.SetTextureNone();
                }
            }
            isBlank = true;
        }

        public void SwitchTo2D()
        {
            // Make the map 2D
            float from = 0;
            if (mapTiles.Length > 0) from = mapTiles[0].GetMaterialProperty(flatnessParameterName);
            SetShaderParameterAnim(flatnessParameterName, toggle3DanimationTime, from, 1);
            // After it finishes, turn of shadows
            animationFinished.AddListener(FinishSettingTo2D);

            is2D = true;
        }

        private void FinishSettingTo2D()
        {
            if (isBlank)
            {
                // Hide the contour texture
                foreach (var m in mapTiles)
                {
                    m.SetTextureBlank();
                }
            }
            SetLit(false);
            Reset2D();
            animationFinished.RemoveListener(FinishSettingTo2D);
        }

        public void SwitchTo3D()
        {
            animationFinished.RemoveListener(FinishSettingTo2D);
            // Set shadows to true
            SetLit(true);
            if (isBlank)
            {
                // Hide the contour texture
                foreach (var m in mapTiles)
                {
                    m.SetTextureNone();
                }
            }
            // Make the map 3D
            float from = 1;
            if (mapTiles.Length > 0) from = mapTiles[0].GetMaterialProperty(flatnessParameterName);
            SetShaderParameterAnim(flatnessParameterName, toggle3DanimationTime, from, 0);
            Reset3D();
            is2D = false;
        }

        private void SetLit(bool lit)
        {
            foreach (var m in mapTiles)
            {
                m.SetLit(lit);
            }
        }

        private void SetShaderParameterAnim(string parameterNameID, float animationTime, float valueFrom, float valueTo)
        {
            if (runningAnimation == parameterNameID)
            {
                // another parameter animation running
                Debug.Log("Could not run parameter animation, while one is already running.");
                return;
            }
            if (mapTiles.Length > 0)
            {
                // cannot run coroutine when GJ is not active
                if (!gameObject.activeInHierarchy || animationTime == 0)
                {
                    SetMaterialProperty(parameterNameID, valueTo);
                }
                else
                {
                    if (animationCoroutine != null)
                    {
                        // interrupt the previous coroutine
                        StopCoroutine(animationCoroutine);
                    }
                    animationCoroutine = ShaderParameterAnimationCoroutine(parameterNameID, valueFrom, valueTo, animationTime);
                    // start the animation
                    StartCoroutine(animationCoroutine);
                }
            }
            else
            {
                Debug.LogError("No renderers found.");
            }
        }

        private void ToggleShaderParameter(string parameterNameID, float animationTime, float min = 0, float max = 1)
        {
            if (runningAnimation == parameterNameID)
            {
                // another parameter animation running
                Debug.Log("Could not run parameter animation, while one is already running.");
                return;
            }
            if (mapTiles.Length > 0)
            {
                // the value should be the same for all renderers
                float changeValueFrom = mapTiles[0].GetMaterialProperty(parameterNameID);
                // if changing from zero, change to 0, otherwise go back to 3D
                float changeValueTo = changeValueFrom == min ? max : min;

                if (gameObject.activeInHierarchy)
                {
                    if (animationCoroutine != null)
                    {
                        // interrupt the previous coroutine
                        StopCoroutine(animationCoroutine);
                        // use the elapsed time from the interupted animation to return
                        animationCoroutine = ShaderParameterAnimationCoroutine(parameterNameID, changeValueFrom, changeValueTo, elapsedAnimationTime);
                    }
                    else
                    {
                        animationCoroutine = ShaderParameterAnimationCoroutine(parameterNameID, changeValueFrom, changeValueTo, animationTime);
                    }
                    // start the animation
                    StartCoroutine(animationCoroutine);
                }
                // cannot run coroutine when GJ is not active
                else
                {
                    SetMaterialProperty(parameterNameID, changeValueTo);
                }
            }
            else
            {
                Debug.LogError("No renderers found.");
            }
        }

        // Toggle parameters in range [0,1] smoothly
        private IEnumerator ShaderParameterAnimationCoroutine(string parameterNameID, float from, float to, float animationTime)
        {
            elapsedAnimationTime = 0;
            while (elapsedAnimationTime < animationTime)
            {
                var value = Mathf.Lerp(from, to, elapsedAnimationTime / animationTime);
                SetMaterialProperty(parameterNameID, value);
                elapsedAnimationTime += Time.deltaTime;
                yield return null;
            }
            // should be always 0 or 1
            SetMaterialProperty(parameterNameID, to);
            // forget the coroutine
            animationCoroutine = null;
            animationFinished.Invoke();
        }


        private void SetMaterialProperty(string parameterNameID, float value)
        {
            // can be done in paralel...
            foreach (var m in mapTiles) m.SetMaterialProperty(parameterNameID, value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value">Percent from 0 to 1.</param>
        public void Crop(float value)
        {
            cropTop = value;
            SetMaterialProperty(cropParameterName, Mathf.Clamp01(value));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value">From 1 to 10.</param>
        public void SetEquidistance(float value)
        {
            float clamped = Mathf.Clamp(value, 1, 10);
            // map from (1, 10) to (0.001, 0.01)
            equidistance = clamped;
            float remapped = 0.001f * clamped;
            SetMaterialProperty(equidistanceParameterName, remapped);
        }

        // TODO correct cropping and distance between lines

    }
}
