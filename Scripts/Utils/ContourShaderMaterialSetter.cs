﻿using UnityEngine;

namespace Edive.Environments.TerrainContours.Utils
{
    public class ContourShaderMaterialSetter : MonoBehaviour
    {
        [SerializeField]
        private Material material;

        [SerializeField]
        private Mesh mesh;

        [SerializeField]
        private string minMaxVectorShaderParameterName = "_MinMaxVertexPosition";

        private void Reset()
        {
            mesh = GetComponent<MeshFilter>().sharedMesh;
            material = GetComponent<Renderer>().sharedMaterial;
        }

        public void SetMinMaxVertexPosition()
        {
            if (mesh?.vertices.Length <= 0)
            {
                return;
            }

            float min = mesh.vertices[0].y;
            float max = mesh.vertices[0].y;

            foreach (Vector3 v in mesh.vertices)
            {
                if (v.y < min)
                {
                    min = v.y;
                }
                if (v.y > max)
                {
                    max = v.y;
                }
            }
            material.SetVector(minMaxVectorShaderParameterName, new Vector2(min, max));
            Debug.Log($"Min set to {min}, max {max}");
        }

    }
}
