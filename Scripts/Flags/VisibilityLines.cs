﻿using System.Collections.Generic;
using UnityEngine;

namespace Edive.Environments.TerrainContours.Flags
{
    public class VisibilityLines : MonoBehaviour
    {
        [SerializeField]
        private GameObject linePrefab;

        [SerializeField]
        private int initialLinePoolSize = 2;

        private Queue<VisibilityLine> disabledLinesPool = new Queue<VisibilityLine>();
        private List<VisibilityLine> enabledLines = new List<VisibilityLine>();

        public void DrawLine(Vector3 a, Vector3 b, Color color)
        {
            DrawLine(a, b, color, color);
        }

        public void DrawLine(Vector3 a, Vector3 b, Color startColor, Color endColor)
        {
            VisibilityLine line = EnableLineAndGetFromPool();
            line.LineStart = a;
            line.LineEnd = b;
            line.StartColor = startColor;
            line.EndColor = endColor;
        }

        private VisibilityLine EnableLineAndGetFromPool()
        {
            VisibilityLine line;
            if (disabledLinesPool.Count > 0)
            {
                line = disabledLinesPool.Dequeue();
            }
            else
            {
                line = Instantiate(linePrefab, transform).GetComponent<VisibilityLine>();
            }
            line.Visible = true;
            enabledLines.Add(line);
            return line;
        }

        private void DisableLineAndAddToPool(VisibilityLine line)
        {
            line.Visible = false;
            disabledLinesPool.Enqueue(line);
        }

        public void DisableAllLines()
        {
            foreach (var line in enabledLines)
            {
                DisableLineAndAddToPool(line);
            }
            enabledLines.Clear();
        }

        private void Awake()
        {
            for (int i = 0; i < initialLinePoolSize; ++i)
            {
                DisableLineAndAddToPool(Instantiate(linePrefab, transform).GetComponent<VisibilityLine>());
            }
        }
    }
}
