﻿using Edive.Interactions.Transformations;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Edive.Environments.TerrainContours.Flags
{
    public class FlagManager : MonoBehaviour
    {
        [SerializeField]
        private Flag[] flags;

        public Flag objectiveA;
        public Flag objectiveB;

        [Tooltip("The layer on which to detect collision (map layer)")]
        public LayerMask intervisibilityLayerMask = -1;

        [SerializeField]
        private VisibilityLines visibilityLines;


        public IEnumerable<Flag> GetFlags()
        {
            return Array.AsReadOnly(flags);
        }

        public bool CheckLineVisibilityThroughAllFlags()
        {
            if (visibilityLines) visibilityLines.DisableAllLines();
            if (!(objectiveA.IsOnTerrain && objectiveB.IsOnTerrain))
            {
                Debug.Log("Objective must be on terrain.");
                return false;
            }
            List<Flag> flagsToBeCheckedSorted = new List<Flag>();
            flagsToBeCheckedSorted.Add(objectiveA);
            foreach (Flag f in flags)
            {
                // do not include flags that are not on terrain; First flag is the first objective, which is already checked if it is on terrain
                if (f.IsOnTerrain)
                {
                    flagsToBeCheckedSorted.Add(f);
                }
            }
            flagsToBeCheckedSorted.Add(objectiveB);

            bool visibleFromAll = true;
            for (int i = 1; i < flagsToBeCheckedSorted.Count; i++)
            {
                bool visibleFromOther = flagsToBeCheckedSorted[i - 1].IsVisibleFrom(flagsToBeCheckedSorted[i], intervisibilityLayerMask);
                if (visibilityLines)
                {
                    visibilityLines.DrawLine(flagsToBeCheckedSorted[i - 1].visibilityPoint.position, flagsToBeCheckedSorted[i].visibilityPoint.position, visibleFromOther ? Color.green : Color.red);
                }
                visibleFromAll = visibleFromAll && visibleFromOther;
            }
            return visibleFromAll;
        }

        public void ResetFlags()
        {
            foreach (Flag f in flags)
            {
                var m = f?.GetComponent<MatchTransform>();
                if (m && m.isActiveAndEnabled) m.Match();
            }
        }
    }
}
